// Dependencies
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

// Pages
import { Home } from "./pages/home";
import { PastTrials } from "./pages/past-trials";
import { HowItWorks } from './pages/how-it-works';
import { Register } from './pages/register';

export const RouterWrapper = () => {
  return (
    <Router>
      <Switch>
        <Route path="/past-trials">
          <PastTrials />
        </Route>
        <Route path="/how-it-works">
          <HowItWorks />
        </Route>
        <Route path="/register">
          <Register />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
};

