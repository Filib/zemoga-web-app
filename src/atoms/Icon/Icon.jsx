// Dependencies
import React from 'react';

// Styles
import { StyledIcon } from "./icon.styles";

export const Icon = ({ name, fontSize, ...iconProps }) => {
    return (
        <StyledIcon className={name} fontSize={fontSize} {...iconProps} />
    );
};

