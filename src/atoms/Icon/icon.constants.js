export const THUMBS_UP = "fa fa-thumbs-up";
export const THUMBS_DOWN = "fa fa-thumbs-down";
export const CLOSE_ICON = "fa fa-times";
export const FACEBOOK = "fa fa-facebook-square";
export const TWITTER = "fa fa-twitter-square";
export const HAMBURGER_MENU = "fas fa-bars";
export const SEARCH = "fas fa-search";