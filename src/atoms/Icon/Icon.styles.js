// Dependencies
import styled from "styled-components";

export const StyledIcon = styled.i`
    font-size: ${({ fontSize }) => fontSize ? fontSize : 12}px;
    color: #fff;
`;