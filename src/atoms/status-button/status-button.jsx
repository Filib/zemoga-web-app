// Dependencies
import React from 'react';

// Styles
import {
    StatusButtonContainer,
    StatusIconRotated
} from "./status-button.styles";

// Constants
import { THUMBS_UP} from "../Icon/icon.constants";

// Atoms
import {Icon} from "../Icon";

export const StatusButton = ({ like = true, active = false, enabled = true, ...statusButtonProps }) => {
    const iconProps = { name: THUMBS_UP, fontSize: 20};

    return (
        enabled && <StatusButtonContainer like={like} active={active} {...statusButtonProps}>
            {like ? <Icon {...iconProps} /> : <StatusIconRotated className={THUMBS_UP} {...iconProps} />}
        </StatusButtonContainer>
    );
};
