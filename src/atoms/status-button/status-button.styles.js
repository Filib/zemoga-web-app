// Dependencies
import styled from "styled-components";

// Atoms
import {Icon} from "../Icon";

export const StatusButtonContainer = styled.button`
    width: 32px;
    height: 32px;
    background-color: ${({ like }) => like ? "#1CBBB4" : "#FFAD1D" };
    border: ${({ active }) => active ? "1px solid #fff" : "0" };
    :focus {
        outline: 0;
    }
    cursor: pointer;
`;

export const StatusIconRotated = styled(Icon)`
    transform: rotate(180deg);
`;
