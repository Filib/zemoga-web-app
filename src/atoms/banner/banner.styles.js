// Dependencies
import styled from "styled-components";
import breakpoint from "../../utils/breakpoint";

export const BannerWrapper = styled.div`
    width: 100%;
    height: 120px;
    background-image: url(${({ backgroundImageUrl }) => backgroundImageUrl});
    margin-bottom: 45px;
    background-size: cover;
    background-repeat: no-repeat;

    ${breakpoint.sm`
        height: 80px;
    `}
`;

export const BannerContainer = styled.div`
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.8);
    color: #333333;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    ${breakpoint.sm`
        flex-direction: row;
        justify-content: space-between;
        padding: 40px 20px;
    `}
`;

export const BannerLeftBlock = styled.div`
    display: flex;
`;

export const BannerDescriptionText = styled.p`
    font-size: 22px;
    padding: 0;
    margin: 0;
    font-weight: 400;
    text-align: center;
    margin-bottom: 20px;

    ${breakpoint.sm`
        font-size: 30px;
        font-weight: 300;
        margin-bottom: 0px;
    `}
`;

export const BannerButtonContainer = styled.div`
    display: flex;
`;

export const BannerButton = styled.button`
    text-align: center;
    width: 172px;
    height: 35px;
    padding: 9px 20px 10px;
    background: transparent;
    margin: 0 auto;
    border: 2px solid #333;
    font-weight: 600;
    cursor: pointer;
    :focus {
        outline: 0;
    }

    ${breakpoint.sm`
        font-size: 15px;
        width: 212px;
        height: 53px;
        border: 3px solid #333;
    `}
`;