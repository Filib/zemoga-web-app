// Dependencies
import React from 'react';

// Styles
import { GlobalWrapper } from "../../global.styles";
import {
    BannerWrapper,
    BannerContainer,
    BannerLeftBlock,
    BannerDescriptionText,
    BannerButtonContainer,
    BannerButton
} from "./banner.styles";

export const Banner = ({ props }) => {
    const { backgroundImageUrl, description, buttonLabel } = props;
    return (
        <GlobalWrapper>
            <BannerWrapper backgroundImageUrl={backgroundImageUrl}>
                <BannerContainer>
                    <BannerLeftBlock>
                        <BannerDescriptionText>
                            {description}
                        </BannerDescriptionText>
                    </BannerLeftBlock>
                    <BannerButtonContainer>
                        <BannerButton>{buttonLabel}</BannerButton>
                    </BannerButtonContainer>
                </BannerContainer>
            </BannerWrapper>
        </GlobalWrapper>
    );
};
