// Dependencies
import React from 'react';

// Styles
import {
    StatusBarWrapper,
    StatusBarContainer,
    StatusBarContent,
    StatusBarPercentage,
    StatusIcon,
    StatusIconRotated
} from "./status-bar.styles";

// Constants
import {THUMBS_UP} from "../Icon/icon.constants";

export const StatusBar = ({ like = true, percentage = 0, ...statusBarProps }) => {
    const iconProps = { name: THUMBS_UP, fontSize: 28 };
    let currentPercentage = percentage;
    if (currentPercentage < 0) {
        currentPercentage = 0;
    } else if (currentPercentage > 100) {
        currentPercentage = 100;
    }

    return (
        <StatusBarWrapper currentPercentage={currentPercentage}>
            <StatusBarContainer like={like} currentPercentage={currentPercentage} {...statusBarProps}>
                <StatusBarContent like={like}>
                    {like ?
                        <StatusIcon className={THUMBS_UP} {...iconProps} /> :
                        <StatusIconRotated className={THUMBS_UP} {...iconProps} />
                    }
                    <StatusBarPercentage>{currentPercentage}%</StatusBarPercentage>
                </StatusBarContent>
            </StatusBarContainer>
        </StatusBarWrapper>
    );
};
