// Dependencies
import styled from "styled-components";

// Atoms
import {Icon} from "../Icon";

export const StatusBarWrapper = styled.div`
    width: ${({ currentPercentage }) => currentPercentage ? currentPercentage : "0"}%;
    height: 50px;
`;

export const StatusBarContainer = styled.div`
    width: 100%;
    height: 100%;
    background-color: ${({ like }) => like ? "rgba(28, 187, 180, 0.7)" : "rgba(255, 173, 29, 0.7)"};
    display: flex;
    align-items: center;
    justify-content: ${({ like }) => like ? "flex-start" : "flex-end"};
`;

export const StatusBarContent = styled.div`
    display: flex;
    flex-direction: ${({ like }) => like ? "row" : "row-reverse"};
    align-items: center;
    margin: 0 10px 0;
`;

export const StatusBarPercentage = styled.p`
    padding: 0;
    margin: 0;
    font-size: 30px;
    font-weight: 400;
    color: #fff;
`;

export const StatusIcon = styled(Icon)`
    margin-right: 10px;
`;

export const StatusIconRotated = styled(Icon)`
    transform: rotate(180deg);
    margin-left: 10px;
`;
