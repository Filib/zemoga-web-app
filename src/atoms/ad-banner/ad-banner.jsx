// Dependencies
import React, {useState} from 'react';

// Styles
import { GlobalWrapper } from "../../global.styles";
import {
    AbBannerContainer,
    AbBannerLeftBlock,
    AbBannerPrimaryTitleText,
    AbBannerSecondaryTitleText,
    AbBannerRightBlock,
    AbBannerDescriptionText,
    AbBannerButtonContainer,
    AbBannerCloseButton,
    AbBannerCloseIcon
} from "./ad-banner.styles";

// Constants
import {CLOSE_ICON} from "../Icon/icon.constants";

export const AdBanner = ({ props }) => {
    const [showBanner, setBannerVisibility] = useState(true);

    const { primaryTitle, secondaryTitle, description } = props;
    const iconProps = { name: CLOSE_ICON, fontSize: 20 };

    return (
        showBanner && <GlobalWrapper>
            <AbBannerContainer>
                <AbBannerLeftBlock>
                    <AbBannerPrimaryTitleText>
                        {primaryTitle}
                    </AbBannerPrimaryTitleText>
                    <AbBannerSecondaryTitleText>
                        {secondaryTitle}
                    </AbBannerSecondaryTitleText>
                </AbBannerLeftBlock>
                <AbBannerRightBlock>
                    <AbBannerDescriptionText>
                        {description}
                    </AbBannerDescriptionText>
                    <AbBannerButtonContainer>
                        <AbBannerCloseButton onClick={() => setBannerVisibility(false)}>
                            <AbBannerCloseIcon className={CLOSE_ICON} {...iconProps} />
                        </AbBannerCloseButton>
                    </AbBannerButtonContainer>
                </AbBannerRightBlock>
            </AbBannerContainer>
        </GlobalWrapper>
    );
};
