// Dependencies
import styled from "styled-components";
import breakpoint from "../../utils/breakpoint";

// Atoms
import {Icon} from "../Icon";

export const AbBannerContainer = styled.div`
    width: 100%;
    height: 180px;
    background-color: #EBEBEB;
    color: #333333;
    display: flex;
    flex-direction: column;
    margin-bottom: 35px;
    padding: 10px;
    position: relative;

    ${breakpoint.sm`
        height: 100px;
        flex-direction: row;
        padding: 0 10px 0 0;
    `}

    ${breakpoint.md`
        padding: 0 15px;
    `}
`;

export const AbBannerLeftBlock = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 20px;

    ${breakpoint.sm`
        width: 250px;
        justify-content: center;
        margin-bottom: 0px;
        margin-right: 15px;
    `}
`;

export const AbBannerPrimaryTitleText = styled.p`
    font-size: 19px;
    padding: 0;
    margin: 0;
    font-weight: 400;

    ${breakpoint.sm`
        font-size: 14px;
    `}

    ${breakpoint.md`
        font-size: 19px;
    `}
`;

export const AbBannerSecondaryTitleText = styled.p`
    font-size: 34px;
    padding: 0;
    margin: 0;
    font-weight: 750;

    ${breakpoint.sm`
        font-size: 26px;
    `}

    ${breakpoint.md`
        font-size: 33px;
    `}
`;

export const AbBannerRightBlock = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export const AbBannerDescriptionText = styled.p`
    font-size: 13px;
    padding: 0;
    margin: 0;
    line-height: 1.5;
`;

export const AbBannerButtonContainer = styled.div`
    position: absolute;
    top: 10px;
    right: 10px;
    margin-left: 0px;

    ${breakpoint.sm`
        position: relative;
        display: flex;
        top: 0px;
        right: 0px;
        margin-left: 10px;
    `}
`;

export const AbBannerCloseButton = styled.button`
    height: 16px;
    width: 16px;
    padding: 0px;
    margin: 0px;
    background: transparent;
    border: 0;
    cursor: pointer;
    :focus {
        outline: 0;
    }
`;

export const AbBannerCloseIcon = styled(Icon)`
    color: #000;
`;