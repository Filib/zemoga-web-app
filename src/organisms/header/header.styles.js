// Dependencies
import styled from "styled-components";
import breakpoint from "../../utils/breakpoint";

// Molecules
import {HeaderCard} from "../../molecules/header-card";

export const HeaderContainer = styled.div`
    width: 100%;
    height: 100vh;
    min-height: 500px;
    background-image: url(${({backgroundImageUrl}) => backgroundImageUrl});
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
    margin-bottom: 35px;
    display: grid;
    grid-template-columns: 1fr;
    grid-template-rows: 80px 1fr 76px;

    ${breakpoint.sm`
        grid-template-rows: 80px 1fr 48px;
    `}
`;

export const HeaderContentMenuWrapper= styled.div`
    width: 100%;
    height: 80px;
    padding-left: 20px;
    padding-right: 20px;
    background-color: rgba(0, 0, 0, 0.1);

    ${breakpoint.sm`
        background-color: transparent;
    `}
`;

export const HeaderContentCardWrapper = styled.div`
    width: 100%;
`;

export const HeaderContentCard = styled(HeaderCard)`
`;

export const HeaderClosingDaysBarContainer = styled.div`
    width: 100%;
    height: 76px;
    display: flex;
    flex-direction: column;

    ${breakpoint.sm`
        flex-direction: row;
        height: 48px;
    `}
`;

export const HeaderClosingInBar = styled.div`
    width: 100%;
    height: 38px;
    background-color: rgba(0, 0, 0, 0.5);
    text-align: left;
    display: flex;
    justify-content: flex-start;
    align-items: center;

    ${breakpoint.sm`
        width: ${({ closingInPercentage }) => closingInPercentage}%;
        height: 100%;
        text-align: right;
        justify-content: flex-end;
    `}
`;

export const HeaderClosingInBarText = styled.p`
    font-size: 15px;
    font-weight: 500;
    color: white;
    margin: 0;
    padding: 0;
    letter-spacing: 0.5px;
    margin-left: 20px;

    ${breakpoint.sm`
        font-size: 11px;
        font-weight: 700;
        margin-left: 0px;
        margin-right: 10px;
    `}
`;

export const HeaderRemainingBar = styled.div`
    width: 100%;
    height: 38px;
    background-color: rgba(255, 255, 255, 0.5);
    text-align: left;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;

    ${breakpoint.sm`
        width: ${({ closingInPercentage }) => closingInPercentage}%;
        height: 100%;
    `}
`;

export const HeaderRemainingBarText = styled.p`
    font-size: 20px;
    font-weight: 500;
    color: rgb(70, 70, 70);
    margin-left: ${({ marginLeft }) => marginLeft ? marginLeft : 0}px;
    margin-right: 5px;
    padding: 0;
    letter-spacing: 0.5px;

    ${breakpoint.sm`
        font-size: 35px;
        font-weight: ${({ fontWeight }) => fontWeight ? fontWeight : 800};
        margin-right: 10px;
    `}
`;