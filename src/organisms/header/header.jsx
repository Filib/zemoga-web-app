// Dependencies
import React from 'react';

// Styles

import {
    HeaderContainer,
    HeaderContentCardWrapper,
    HeaderContentMenuWrapper,
    HeaderContentCard,
    HeaderClosingDaysBarContainer,
    HeaderClosingInBar,
    HeaderRemainingBar,
    HeaderClosingInBarText,
    HeaderRemainingBarText
} from "./header.styles";

// Molecules
import {Menu} from "../../molecules/menu";

export const Header = ({props}) => {
    const {
        firstTitle,
        secondTitle,
        content,
        link,
        backgroundImageUrl,
        remainingDays,
        totalDays
    } = props;
    const closingInPercentage = ((totalDays - remainingDays)/100) * 100;
    const remainingPercentage = 100 - closingInPercentage;
    const headerContentCardProps = { firstTitle, secondTitle, content, link};

    return (
        <HeaderContainer backgroundImageUrl={backgroundImageUrl}>
            <HeaderContentMenuWrapper>
                <Menu />
            </HeaderContentMenuWrapper>
            <HeaderContentCardWrapper>
                <HeaderContentCard props={headerContentCardProps} />
            </HeaderContentCardWrapper>
            <HeaderClosingDaysBarContainer>
                <HeaderClosingInBar closingInPercentage={closingInPercentage}>
                    <HeaderClosingInBarText
                        closingInPercentage={closingInPercentage}>
                        CLOSING IN
                    </HeaderClosingInBarText>
                </HeaderClosingInBar>
                <HeaderRemainingBar remainingPercentage={remainingPercentage}>
                    <HeaderRemainingBarText fontWeight={500} marginLeft={20}>{remainingDays}</HeaderRemainingBarText>
                    <HeaderRemainingBarText fontWeight={300}>days</HeaderRemainingBarText>
                </HeaderRemainingBar>
            </HeaderClosingDaysBarContainer>
        </HeaderContainer>
    );
};
