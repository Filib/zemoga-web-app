// Dependencies
import React from 'react';

import {
    CardsContainer,
    CardsTitle,
    CardsGrid
} from "./cards.styles";

// Molecules
import {Card} from "../../molecules/card";

export const Cards = ({ props }) => {
    const {cards} = props;
    return (
        <CardsContainer>
            <CardsTitle> Votes </CardsTitle>
            <CardsGrid>
                {cards.map((card) => {
                    return <Card props={card} />
                })}
            </CardsGrid>
        </CardsContainer>
    );
};
