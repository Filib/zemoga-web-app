// Dependencies
import styled from "styled-components";
import breakpoint from "../../utils/breakpoint";

export const CardsContainer = styled.div`
    margin-bottom: 40px;
`;

export const CardsTitle = styled.h2`
    font-size: 30px;
    margin-bottom: 30px;
    font-weight: 300;
`;

// export const CardsGrid = styled.div`
//     display: flex;
//     flex-wrap: wrap;
//     flex: 1;
//     gap: 40px;
// `;

export const CardsGrid = styled.div`
    display: grid;
    grid-template-columns: 1fr;
    gap: 20px;

    ${breakpoint.sm`
        grid-template-columns: 1fr 1fr;
        gap: 40px;
    `}
`;