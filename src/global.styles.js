// Dependencies
import styled from "styled-components";

// max-width: 1000px;
export const GlobalWrapper = styled.div`
    margin: 0 auto;
    max-width: 1000px;
    width: calc(100vw - 40px);
`;