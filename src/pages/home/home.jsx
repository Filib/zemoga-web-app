// Dependencies
import React from 'react';

// Styles
import {
    GlobalWrapper
} from "../../global.styles";

import {
    HomeContainer,
    HeaderComponent,
    AdBannerComponent,
    BannerComponent,
    DottedLine
} from "./home.styles";

// Molecules
import { Footer } from "../../molecules/footer";

// Organisms
import { Cards } from "../../organisms/cards";

// Mock Data
import mockData from "../../data/mock-data.json";

export const Home = () => {
    const { headerBlock, adBannerBlock, bannerBlock, cardsBlock} = mockData;
    const cardsProps = { cards: cardsBlock};
    return (
        <HomeContainer>
            <HeaderComponent props={headerBlock}/>
            <AdBannerComponent props={adBannerBlock}/>
            <GlobalWrapper>
                <Cards props={cardsProps} />
            </GlobalWrapper>
            <BannerComponent props={bannerBlock}/>
            <GlobalWrapper>
                <DottedLine />
                <Footer />
            </GlobalWrapper>
        </HomeContainer>
    );
};