// Dependencies
import styled from "styled-components";

// Atoms
import {AdBanner} from "../../atoms/ad-banner";
import {Banner} from "../../atoms/banner";

// Organisms
import { Header } from "../../organisms/header";

// External Styles
import { HeaderContainer} from "../../organisms/header/header.styles";

export const HomeContainer = styled.div`
    width: 100%;
    height: 100%;
`;

export const HeaderComponent = styled(Header)`
    ${HeaderContainer}{
        margin-bottom: 35px;
    }
`;

export const AdBannerComponent = styled(AdBanner)`
    margin-bottom: 30px;
`;

export const BannerComponent = styled(Banner)`
    margin-bottom: 30px;
`;

export const DottedLine = styled.div`
    width: 100%;
    height: 1px;
    background-image: url("data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' stroke='gray' stroke-width='4' stroke-dasharray='2%2c 4' stroke-dashoffset='0' stroke-linecap='butt'/%3e%3c/svg%3e");
    margin-bottom: 30px;
`;