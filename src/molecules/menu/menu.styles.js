// Dependencies
import styled from "styled-components";
import breakpoint from "../../utils/breakpoint";

// Atoms
import { Icon } from "../../atoms/Icon";

export const MenuContainer = styled.div`
    width: 100%;
    height: 100%;
    color: #fff;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const MenuSiteName = styled.a`
    padding: 0;
    margin: 0;
    font-size: 25px;
    text-decoration: none;
    color: #fff;
`;

export const MenuNavContainer = styled.nav`
`;

export const MenuNav = styled.ul`
    padding: 0;
    margin: 0;
    list-style: none;
    position: absolute;
    left: ${({menuIsOpen}) => menuIsOpen ? "0" : "-500px"};
    ${({menuIsOpen}) => menuIsOpen ? "right: 0;" : ""}
    bottom: 0;
    top: 80px;
    background-color: rgba(255, 255, 255, 0.7);
    color: rgba(0, 0, 0, 0.87);
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    transition: 300ms ease;

    ${breakpoint.sm`
        flex-direction: row;
        height: 48px;
        position: static;
        background-color: transparent;
        color: #ffffff;
    `}
`;

export const MenuNavItem = styled.li`
    padding: 10px;
    margin: 0;
`;

export const MenuNavLink = styled.a`
    font-size: 15px;
    text-decoration: none;
    color: #000;
`;

export const MenuIcon = styled(Icon)`
    font-size: 25px;
    color: #FFF;
    cursor: pointer;

    ${breakpoint.sm`
        display: none;
    `}
`;

export const MenuSearchIcon = styled(Icon)`
    font-size: 25px;
    color: inherit;
    cursor: pointer;
`;
