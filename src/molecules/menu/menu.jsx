// Dependencies
import React, {useState} from 'react';

// Styles
import {
    MenuContainer,
    MenuSiteName,
    MenuIcon,
    MenuNavContainer,
    MenuNav,
    MenuNavItem,
    MenuNavLink,
    MenuSearchIcon
} from "./menu.styles";

// Constants
import { HAMBURGER_MENU, SEARCH } from "../../atoms/Icon/icon.constants";

export const Menu = ({ props }) => {
    const [isMenuOpen, setIsMenuOpen] = useState(false)

    const handleMenuOpen = (e) => {
        e.preventDefault()
        setIsMenuOpen(!isMenuOpen)
    }

    return (
        <MenuContainer>
            <MenuSiteName href="#">Rule of Thumb</MenuSiteName>
            <MenuNavContainer>
                <MenuIcon className={HAMBURGER_MENU} onClick={handleMenuOpen} />
                    <MenuNav menuIsOpen={isMenuOpen}>
                    <MenuNavItem>
                        <MenuNavLink href="/past-trials">Past Trials</MenuNavLink>
                    </MenuNavItem>
                    <MenuNavItem>
                        <MenuNavLink href="/how-it-works">How It Works</MenuNavLink>
                    </MenuNavItem>
                    <MenuNavItem>
                        <MenuNavLink href="/register">Log in/Sign up</MenuNavLink>
                    </MenuNavItem>
                    <MenuNavItem>
                        <MenuSearchIcon className={SEARCH}/>
                    </MenuNavItem>
                    </MenuNav>
            </MenuNavContainer>
        </MenuContainer>
    );
};
