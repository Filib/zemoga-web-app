// Dependencies
import React from 'react';

// Styles
import {
    GlobalWrapper
} from "../../global.styles";

import {
    FooterContainer,
    FooterLinksList,
    FooterListItem,
    FooterLink,
    FooterSocialLinksList,
    FooterSocialLink,
    FacebookIcon,
    TwitterIcon
} from "./footer.styles";

// Constants
import {FACEBOOK, TWITTER} from "../../atoms/Icon/icon.constants";

export const Footer = ({ props }) => {
    const facebookIconProps = { name: FACEBOOK};
    const twitterIconProps = { name: TWITTER};

    return (
        <GlobalWrapper>
            <FooterContainer>
                <FooterLinksList>
                    <FooterListItem>
                        <FooterLink href="#">
                            Terms and Conditions
                        </FooterLink>
                    </FooterListItem>
                    <FooterListItem>
                        <FooterLink href="#">
                            Privacy Policy
                        </FooterLink>
                    </FooterListItem>
                    <FooterListItem>
                        <FooterLink href="#">
                            Contact Us
                        </FooterLink>
                    </FooterListItem>
                </FooterLinksList>
                <FooterSocialLinksList>
                    <FooterSocialLink>
                        <FooterLink href="#">
                            Follow Us
                        </FooterLink>
                    </FooterSocialLink>
                    <FooterSocialLink>
                        <FooterLink href="https://facebook.com">
                            <FacebookIcon className={FACEBOOK} {...facebookIconProps} />
                        </FooterLink>
                    </FooterSocialLink>
                    <FooterSocialLink>
                        <FooterLink href="https://twitter.com">
                            <TwitterIcon className={TWITTER} {...twitterIconProps} />
                        </FooterLink>
                    </FooterSocialLink>
                </FooterSocialLinksList>
            </FooterContainer>    
        </GlobalWrapper>
    );
};
