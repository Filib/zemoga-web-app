// Dependencies
import styled from "styled-components";
import breakpoint from "../../utils/breakpoint";

// Atoms
import {Icon} from "../../atoms/Icon";

export const FooterContainer = styled.div`
    width: 100%;
    height: 80px;
    margin-bottom: 30px;
    display: flex;
    flex-direction: column;

    ${breakpoint.sm`
        flex-direction: row;
        height: 40px;
    `}
`;

export const FooterLinksList = styled.ul`
    padding: 0;
    margin: 0 0 30px 0;
    list-style-type: none;
    display: flex;
    justify-content: space-around;
    align-items: center;

    ${breakpoint.sm`
        margin-bottom: 0px;
        width: 50%;
        justify-content: flex-start;
    `}
`;

export const FooterListItem = styled.li`
    display: inline-block;
    margin-right: 0px;
    
    ${breakpoint.sm`
        margin-right: 20px;
    `}
`;

export const FooterLink = styled.a`
    text-decoration: none;
    font-size: 15px;
    color: #333;
    
    ${breakpoint.sm`
        font-size: 12px;
    `}
`;

export const FooterSocialLinksList = styled.ul`
    padding: 0;
    margin: 0;
    list-style-type: none;
    display: flex;
    justify-content: space-evenly;
    align-items: center;

    ${breakpoint.sm`
        margin-bottom: 0px;
        width: 50%;
        justify-content: flex-end;
    `}
`;

export const FooterSocialLink = styled.li`
    font-size: 10px;
    color: #333;
    display: inline-block;
    margin-left: 0px;

    ${breakpoint.sm`
        margin-left: 20px;
    `}
`;

export const FacebookIcon = styled(Icon)`
    font-size: 30px;
    color: #000;
    cursor: pointer;
`;

export const TwitterIcon = styled(Icon)`
    font-size: 30px;
    color: #000;
    cursor: pointer;
`;
