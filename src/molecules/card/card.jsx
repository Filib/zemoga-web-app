// Dependencies
import React, { useState, useEffect } from 'react';

// Styles
import {
    CardContainer,
    CardStatus,
    CardStatusBarsContainer,
    CardDetailsContainer,
    CardPersonName,
    CardPersonBusiness,
    CardContent,
    CardButtonsContainer,
    CardLikeButton,
    CardButton
} from "./card.styles";

// Atoms
import { StatusBar } from "../../atoms/status-bar";

export const Card = ({ props }) => {
    const {
        name,
        period,
        business,
        description,
        backgroundImageUrl,
        upVotes,
        downVotes
    } = props;

    const LIKE_BUTTON = "like";
    const DISLIKE_BUTTON = "dislike";
    const UP_VOTES = "upVotes";
    const DOWN_VOTES = "downVotes";
    const THANKS_FOR_VOTING = "Thank you for voting!";

    const initialVotesState = () => {
        const data = JSON.parse(localStorage.getItem(name));
        if (data) {
            return { upVotes: data.upVotes, downVotes: data.downVotes };
        }
        return { upVotes: upVotes, downVotes: downVotes };
    };

    const [activeButton, setButtonsState] = useState({ like: false, dislike: false });
    const [enableContent, setDetailsContent] = useState({
        descriptionSection: true,
        votesSection: true,
        voteAgainButton: false
    });
    const [votes, setVotes] = useState(initialVotesState());

    const updateButtonsState = (buttonType) => {
        if (buttonType === LIKE_BUTTON) {
            setButtonsState({ like: !activeButton.like, dislike: false });
        } else if (buttonType === DISLIKE_BUTTON) {
            setButtonsState({ like: false, dislike: !activeButton.dislike });
        } else {
            setButtonsState({ like: false, dislike: false });
        }
    };

    const updateContentVisibility = () => {
        setDetailsContent({
            descriptionSection: false,
            votesSection: !enableContent.votesSection,
            voteAgainButton: !enableContent.voteAgainButton
        });
    };

    const vote = () => {
        if (activeButton.like || activeButton.dislike) {
            setVotes(
                {
                    upVotes: activeButton.like ? votes.upVotes + 1 : votes.upVotes,
                    downVotes: activeButton.dislike ? votes.downVotes + 1 : votes.downVotes
                }
            )
            updateButtonsState();
            updateContentVisibility();
        }
    };

    useEffect(() => {
        if (enableContent.voteAgainButton) {
            localStorage.setItem(name, JSON.stringify(votes));
        }
    });

    const calculatePercentage = (votesType, amount) => {
        if (votes.upVotes === 0 && votes.downVotes === 0) {
            return 0;
        } else if (votesType === UP_VOTES && votes.downVotes === 0) {
            return 100;
        } else if (votesType === DOWN_VOTES && votes.upVotes === 0) {
            return 100;
        }
        const total = votes.upVotes + votes.downVotes;
        return Math.round((amount / total) * 100);
    };

    return (
        <CardContainer backgroundImageUrl={backgroundImageUrl}>
            <CardStatus
                like={votes.upVotes > votes.downVotes}
                enabled={votes.upVotes !== votes.downVotes}
            />
            <CardStatusBarsContainer>
                <StatusBar
                    like={true}
                    percentage={calculatePercentage(UP_VOTES, votes.upVotes)}
                />
                <StatusBar
                    like={false}
                    percentage={calculatePercentage(DOWN_VOTES, votes.downVotes)}
                />
            </CardStatusBarsContainer>
            <CardDetailsContainer>
                <CardPersonName>{name}</CardPersonName>
                <CardPersonBusiness><strong>{period}</strong> in {business}</CardPersonBusiness>
                <CardContent>{enableContent.descriptionSection ? description : THANKS_FOR_VOTING}</CardContent>
                <CardButtonsContainer>
                    <CardLikeButton
                        active={activeButton.like}
                        like={true}
                        onClick={() => updateButtonsState(LIKE_BUTTON)}
                        enable={enableContent.votesSection}
                    />
                    <CardLikeButton
                        active={activeButton.dislike}
                        like={false}
                        onClick={() => updateButtonsState(DISLIKE_BUTTON)}
                        enable={enableContent.votesSection}
                    />
                    <CardButton
                        onClick={() => vote()}
                        enable={enableContent.votesSection}
                    >Vote now
                    </CardButton>
                    <CardButton
                        onClick={() => updateContentVisibility()}
                        enable={enableContent.voteAgainButton}
                    >Vote again
                    </CardButton>
                </CardButtonsContainer>
            </CardDetailsContainer>
        </CardContainer>
    );
};
