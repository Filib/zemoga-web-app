// Dependencies
import styled from "styled-components";

// Atoms
import { StatusButton } from "../../atoms/status-button";

export const CardContainer = styled.div`
    width: 100%;
    height: 537px;
    background-image:
        linear-gradient(to bottom, rgba(255, 255, 255, 0.1), rgba(0, 0, 0, 0.5)),
        url(${({ backgroundImageUrl }) => backgroundImageUrl});
    background-repeat: no-repeat;
    background-position: center right;
    background-size: cover;
        padding: 50px;
    display: flex;
    align-items: flex-end;
    position: relative;
    padding: 50px;
    position: relative;
`;

export const CardStatus = styled(StatusButton)`
    position: absolute;
    bottom: 246px;
    left: 0;
    cursor: auto;
`;

export const CardStatusBarsContainer = styled.div`
    position: absolute;
    bottom: 0px;
    right: 0px;
    left: 0px;
    display: flex;
    justify-content: space-between;
`;

export const CardDetailsContainer = styled.div`
    display: flex;
    flex-direction: column;
    padding: 0;
    margin: 0 0 65px 0;
`;

export const CardPersonName = styled.p`
    padding: 0;
    margin: 0;
    color: #fff;
    font-size: 40px;
`;

export const CardPersonBusiness = styled.p`
    padding: 0;
    margin: 0 0 15px 0;
    color: #fff;
    font-size: 12px;
`;

export const CardContent = styled.p`
    padding: 0;
    margin: 0;
    color: #fff;
    font-size: 16px;
    margin-bottom: 20px;
`;

export const CardButtonsContainer = styled.div`
    display: flex;
    align-items: center;
`;

export const CardLikeButton = styled(StatusButton)`
    margin-right: 15px;
    display: ${({ enable }) => enable ? "block" : "none" };
`;

export const CardButton = styled.button`
    width: 115px;
    height: 40px;
    border: 2px solid #fff;
    padding: 6px 20px 10px;
    font-size: 14px;
    background-color: transparent;
    color: #fff;
    font-weight: 500;
    cursor: pointer;
    :focus {
        outline: 0;
    }
    display: ${({ enable }) => enable ? "block" : "none" };
`;
