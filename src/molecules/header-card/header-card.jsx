// Dependencies
import React from 'react';

// Styles
import {
    HeaderCardContainer,
    HeaderCardContent,
    HeaderCardFirstTitle,
    HeaderCardSecondTitle,
    HeaderCardDescription,
    HeaderCardLink,
    HeaderCardText,
    HeaderCardBottom,
    HeaderCardOption,
    HeaderCardUpIcon,
    HeaderCardDownIcon
} from "./header-card.styles";

// Constants
import { THUMBS_UP } from "../../atoms/Icon/icon.constants";

export const HeaderCard = ({ props }) => {
    const {firstTitle, secondTitle, content, link} = props;

    return (
        <HeaderCardContainer>
            <HeaderCardContent>
                <HeaderCardFirstTitle>
                    {firstTitle}
                </HeaderCardFirstTitle>
                <HeaderCardSecondTitle>
                    {secondTitle}
                </HeaderCardSecondTitle>
                <HeaderCardDescription>
                    {content}
                </HeaderCardDescription>
                <HeaderCardLink href={link.url}>{link.label}</HeaderCardLink>
                <HeaderCardText>What's Your Veredict?</HeaderCardText>
            </HeaderCardContent>
            <HeaderCardBottom>
                <HeaderCardOption like={true}>
                    <HeaderCardUpIcon className={THUMBS_UP} />
                </HeaderCardOption>
                <HeaderCardOption like={false}>
                    <HeaderCardDownIcon className={THUMBS_UP} />
                </HeaderCardOption>
            </HeaderCardBottom>
        </HeaderCardContainer>
    );
};
