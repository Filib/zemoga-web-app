// Dependencies
import styled from "styled-components";
import breakpoint from "../../utils/breakpoint";

// Atoms
import { Icon } from "../../atoms/Icon";

export const HeaderCardContainer = styled.div`
    width: 100%;
    height: 420px;
    background: #2b2b2b7d;
    color: #fff;
    display: grid;
    grid-template: 1fr 60px / 1fr;

    ${breakpoint.sm`
        max-width: 520px;
        height: 490px;
        margin: 70px 0 0 130px;
        grid-template: 1fr 80px / 1fr;
    `}
`;

export const HeaderCardContent = styled.div`
    height: 100%;
    padding-top: 20px;
    padding-left: 20px;
    padding-right: 20px;

    ${breakpoint.sm`
        padding-top: 45px;
        padding-left: 45px;
        padding-right: 45px;
    `}
`;

export const HeaderCardFirstTitle = styled.p`
    padding: 0;
    margin: 0;
    font-size: 13px;
    color: rgba(255, 255, 255, 0.7);
    letter-spacing: 0.5px;

    ${breakpoint.sm`
        font-size: 15px;
    `}
`;

export const HeaderCardSecondTitle = styled.p`
    padding: 0;
    margin: 0 0 0 -4px;
    font-size: 55px;
    margin-bottom: 20px;

    ${breakpoint.sm`
        font-size: 65px;
    `}
`;

export const HeaderCardDescription = styled.p`
    padding: 0;
    margin: 0;
    font-size: 18px;
    color: rgba(255, 255, 255, 0.7);
    margin-bottom: 25px;

    ${breakpoint.sm`
        font-size: 22px;
    `}
`;

export const HeaderCardLink = styled.a`
    font-size: 13px;
    color: #fff;
    display: inline-block;
    margin-bottom: 40px;

    ${breakpoint.sm`
        font-size: 15px;
    `}
`;

export const HeaderCardText = styled.p`
    padding: 0;
    margin: 0;
    font-size: 20px;
    font-weight: 700;

    ${breakpoint.sm`
        font-size: 22px;
    `}
`;

export const HeaderCardBottom = styled.div`
    height: 60px;
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    bottom: 0;
    right: 0;
    left: 0;

    ${breakpoint.sm`
        height: 80px;
    `}
`;

export const HeaderCardOption = styled.div`
    height: 100%;
    width: 100%;
    background-color: ${({ like }) => like ? "rgba(28, 187, 180, 0.7)" : "rgba(255, 173, 29, 0.7)"};
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const HeaderCardUpIcon = styled(Icon)`
    font-size: 32px;
`;

export const HeaderCardDownIcon = styled(Icon)`
    font-size: 32px;
    transform: rotate(180deg);
`;
